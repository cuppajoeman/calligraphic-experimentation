binaries=scratch
all: 
	g++ calligraphic.cpp -o calligraphic $(shell pkg-config --libs --cflags cairo) -lm
.PHONY: clean
clean:
	rm -f $(binaries)
